#include "ipc.h"
#include "pa1.h"
#include "extra_ipc.h"
#include <unistd.h>
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <sys/wait.h>
#include "pipes.h"
#include "logs.h"

local_id PROC_ID = PARENT_ID;

void child( int n) {
    
    int **pipes = select_pipes(PROC_ID);
    int *read_pipes = pipes[0];
    int *write_pipes = pipes [1];
    
    close_pipes(n, PROC_ID);
    
  	logging(log_started_fmt, PROC_ID,getpid(),getppid());
	
	int count_STARTED_messages = 0;
    int count_DONE_messages = 0;
    Message msg; 
    
    int payload_len = sprintf(msg.s_payload,log_started_fmt, PROC_ID,getpid(),getppid());

    init_message_header(&msg, payload_len, STARTED, 0);

    send_multicast(write_pipes, &msg);

    while (count_STARTED_messages < n - 1) {

        receive_any(read_pipes, &msg);

        if (msg.s_header.s_type == STARTED) count_STARTED_messages++;
        else if (msg.s_header.s_type == DONE) count_DONE_messages++;
    }
	logging(log_received_all_started_fmt, PROC_ID);
    
   	logging(log_done_fmt, PROC_ID);
    
    payload_len = sprintf(msg.s_payload,log_done_fmt, PROC_ID);
 
    init_message_header(&msg, payload_len, DONE, 0);

    send_multicast(write_pipes, &msg);

    while (count_DONE_messages < n - 1) {
        receive_any(read_pipes, &msg);
        if (msg.s_header.s_type == DONE) count_DONE_messages++;
    }

  	logging(log_received_all_done_fmt, PROC_ID);

}

void parent(int n) {
    if (open_logfiles())
        exit(-1);

    init_pipes_buffer(n);

    for (local_id i = 1; i <= n; i++) {
        if (!fork()) {
            PROC_ID = i;
            break;
        }
    }

    if (PROC_ID != 0) {
        child(n);
    } else {
        int **pipes = select_pipes(PROC_ID);
        int *read_pipes = pipes[0];
       
        close_pipes(n, PROC_ID);

        Message msg;
        int count_STARTED_messages = 0;
        int count_DONE_messages = 0;
        while (count_STARTED_messages < n) {

            receive_any(read_pipes, &msg);

            if (msg.s_header.s_type == STARTED) count_STARTED_messages++;
            else if (msg.s_header.s_type == DONE) count_DONE_messages++;

        }

       logging(log_received_all_started_fmt, PROC_ID);

       while (count_DONE_messages < n) {
            receive_any(read_pipes, &msg);
            if (msg.s_header.s_type == DONE) count_DONE_messages++;
        }

        logging(log_received_all_done_fmt, PROC_ID);
		
        while(wait(NULL) > 0);
    }

}

int main (int argc, char *argv[]) {
    if (argc == 3 && strcmp(argv[1], "-p") == 0) {
        int n = atoi(argv[2]);
        if (n >= 1 && n <= 10) {
            parent(n);
        }
    }
    return 0;
}


