#ifndef DIST_COMP_LAB_1_EXTRA_IPC_H
#define DIST_COMP_LAB_1_EXTRA_IPC_H

#include "ipc.h"

void init_message_header(Message *msg, uint16_t  payload_len, MessageType type, timestamp_t local_time);

int receive_all(void * self, Message * msg);
#endif
